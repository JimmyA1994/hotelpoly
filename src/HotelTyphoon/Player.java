package HotelTyphoon;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;

public class Player {

    private static double defaultAvatarRadius = 6.0f;
    private String playerName;
    private Label playerNameLabel;
    private int playerMoney;
    private int playerPeakMoney;
    private Label playerMoneyLabel;
    private int playerRow;
    private int playerCol;
    private String color;
    private Circle playerAvatar;
    private boolean hasPassedTheBank;
    private boolean hasPassedTheCityHall;
    private Background activeNameBackground;
    private Background activeMoneyBackground;

    public Player(String playerName, int playerMoney, int playerRow, int playerCol, String color) {
        this.color = color;
        this.activeNameBackground = new Background(new BackgroundFill(Paint.valueOf(this.color), new CornerRadii(8.0f), new Insets(0,-10,0,-3)));
        this.activeMoneyBackground = new Background(new BackgroundFill(Paint.valueOf(this.color), new CornerRadii(8.0f), new Insets(0,-3,0,0)));
        this.playerName = playerName;
        this.playerNameLabel = new Label();
        this.playerNameLabel.setText(this.playerName+":");
        this.playerNameLabel.setBackground(activeNameBackground);
        this.playerMoney = playerMoney;
        this.playerPeakMoney = playerMoney;
        this.playerMoneyLabel = new Label();
        this.playerMoneyLabel.setText(Integer.toString(this.playerMoney));
        this.playerMoneyLabel.setTextAlignment(TextAlignment.LEFT);
        this.playerMoneyLabel.setBackground(activeMoneyBackground);
        this.playerRow = playerRow;
        this.playerCol = playerCol;
        this.playerAvatar = new Circle(0, 0, defaultAvatarRadius, Paint.valueOf(color));
        this.hasPassedTheBank = false;
        this.hasPassedTheCityHall = false;
    }

    public String getPlayerName() { return playerName; }

    public Label getPlayerNameLabel() { return playerNameLabel; }

    public int getPlayerMoney() { return playerMoney; }

    public void setPlayerMoney(int playerMoney) {
        this.playerMoney = playerMoney;
        this.playerMoneyLabel.setText(Integer.toString(this.playerMoney));
    }

    public int getPlayerPeakMoney() {
        return playerPeakMoney;
    }

    public void setPlayerPeakMoney(int playerPeakMoney) {
        this.playerPeakMoney = playerPeakMoney;
    }

    public Label getPlayerMoneyLabel() { return this.playerMoneyLabel; }

    public int getPlayerRow() {
        return playerRow;
    }

    public void setPlayerRow(int playerRow) {
        this.playerRow = playerRow;
    }

    public int getPlayerCol() {
        return playerCol;
    }

    public void setPlayerCol(int playerCol) {
        this.playerCol = playerCol;
    }

    public Circle getPlayerAvatar() {
        return playerAvatar;
    }

    public void setPlayerAvatar(Circle playerAvatar) {
        this.playerAvatar = playerAvatar;
    }

    public boolean getPlayerMoneyRequestEligibility() {
        return this.hasPassedTheBank;
    }

    public void setPlayerMoneyRequestEligibility( boolean flag){
        hasPassedTheBank = flag;
    }

    public boolean getPlayerBuildingEligibility() {
        return this.hasPassedTheCityHall;
    }

    public void setPlayerBuildingEligibility( boolean flag){
        hasPassedTheCityHall = flag;
    }

    public String getColor() { return this.color; }

    public void playerTurn(){
        this.playerNameLabel.setTextFill(Paint.valueOf("#FFFFFF"));
        this.playerMoneyLabel.setTextFill(Paint.valueOf("#FFFFFF"));
    }

    public void notPlayerTurn(){
        this.playerNameLabel.setTextFill(Paint.valueOf("#000000"));
        this.playerMoneyLabel.setTextFill(Paint.valueOf("#000000"));
    }

    public void PlayerIsOff() {

        Background nameBackground = new Background(new BackgroundFill(Paint.valueOf("#000000"), new CornerRadii(8.0f), new Insets(0,-10,0,-3)));
        Background moneyBackground = new Background(new BackgroundFill(Paint.valueOf("#000000"), new CornerRadii(8.0f), new Insets(0,-3,0,0)));

        this.playerNameLabel.setBackground(nameBackground);
        this.playerMoneyLabel.setBackground(moneyBackground);

        this.playerNameLabel.setTextFill(Paint.valueOf("#000000"));
        this.playerMoneyLabel.setTextFill(Paint.valueOf("#000000"));
    }
}

package HotelTyphoon;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Paint;

import java.util.HashMap;

public class Colors {

    public static String blueColor = "#367BF0";
    public static String redColor = "#B7164B";
    public static String greenColor = "#16A085";

    private static HashMap<Integer,String> colors; // 20 entry HashMap to quickly pick a color

    Colors(){
        colors = new HashMap<>();
        colors.put(0, "#FF8C00");
        colors.put(1, "#CB0FCB");
        colors.put(2, "#E0874A");
        colors.put(3, "#E9BC4C");
        colors.put(4, "#EB5573");
        colors.put(5, "#608FE5");
        colors.put(6, "#7FFFD4");
        colors.put(7, "#C75364");
        colors.put(8, "#ADFF2F");
        colors.put(9, "#E1E146");
        colors.put(10, "#9DD49D");
        colors.put(11, "#4FB97E");
        colors.put(12, "#E0FFFF");
        colors.put(13, "#BA55D3");
        colors.put(14, "#FF1493");
        colors.put(15, "#DF8C7B");
        colors.put(16, "#C586E1");
        colors.put(17, "#3030EC");
        colors.put(18, "#3CA93C");
        colors.put(19, "#CB1A8A");
    }

    public String getColor(Integer number){
        return colors.get(number);
    }

    public Paint getBluePaint() {return Paint.valueOf(this.blueColor);}

    public Paint getRedPaint() {return Paint.valueOf(this.redColor);}

    public Paint getGreenPaint() {return Paint.valueOf(this.greenColor);}

    public Background getBlueBackground() {return new Background(new BackgroundFill(this.getBluePaint(), CornerRadii.EMPTY, Insets.EMPTY));}

    public Background getRedBackground() {return new Background(new BackgroundFill(this.getRedPaint(), CornerRadii.EMPTY, Insets.EMPTY));}

    public Background getGreenBackground() {return new Background(new BackgroundFill(this.getGreenPaint(), CornerRadii.EMPTY, Insets.EMPTY));}

}

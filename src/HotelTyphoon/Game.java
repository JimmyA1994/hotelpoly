package HotelTyphoon;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class Game {

    public static Colors colors;

    private GridPane grid;
    private Region[][] gridElements;
    private String[][] map;

    private Player[] players;
    private int[] playingOrder;
    private int[] initialOrder;
    private boolean[] isActivePlayer;
    private int activePlayers;
    private int whoPlaysIndex;

    private enum Direction {
        upwards, rightwards, downwards, leftwards
    }

    private Direction [] direction;
    private Direction hypotheticalDirection;
    private int startRow;
    private int startCol;

    Deque<Integer> hotels;
    private int availableHotels = -1;
    private Label availableHotelsLabel;
    private HashMap<Integer, String> hotelNames;
    private HashMap<Integer, TitledPane> hotelInfo;
    private HashMap<Integer, Integer> holdingsValue;
    private HashMap<Integer, Integer> purchasePrices;
    private HashMap<Integer, Integer> entrancesPrice;
    private HashMap<Integer, Integer> basicBuildingsCost;
    private HashMap<Integer, Integer> dailyStayPrices;
    private HashMap<Integer, ArrayList<Integer>> upgradesCost;
    private HashMap<Integer, ArrayList<Integer>> upgradedDailyStayPrices;
    private HashMap<Integer, Integer> outdoorPremisesCost;
    private HashMap<Integer, Integer> holdingOwnedBy;
    private HashMap<Integer, Integer> finalDailyStayPrices;
    private HashMap<Integer, Integer> hotelUpgrades;
    private ArrayList<ArrayList<Integer>> playerOwnings;
    private HashMap<Pair<Integer,Integer>,ArrayList<Integer>> entranceAdjacentHotels;
    private HashMap<Integer,ArrayList<Pair<Integer,Integer>>> hotelEntrances;
    private HashMap<Pair<Integer,Integer>,Integer> boughtEntrances;
    private HashMap<Pair<Integer,Integer>,Integer> boughtEntranceForHotel;
    private HashMap<Integer,ArrayList<Pair<Integer,Integer>>> playerEntrances;

    Game(){

        colors = new Colors();
        this.map = new String[12][15];
        this.gridElements = new Region[12][15];
        this.playingOrder = new int [3];
        this.initialOrder = new int [3];
        this.players = new Player [3];
        this.isActivePlayer = new boolean[3];
        this.direction = new Direction[3];
        this.availableHotelsLabel = new Label();

        players[0] = new Player("Player 1", 12000, -1, -1, colors.blueColor);
        players[1] = new Player("Player 2", 12000, -1, -1, colors.redColor);
        players[2] = new Player("Player 3", 12000, -1, -1, colors.greenColor);

        isActivePlayer[0] = true;
        isActivePlayer[1] = true;
        isActivePlayer[2] = true;

        activePlayers = 3;

        startRow = -1;
        startCol = -1;

        this.hotels = new ArrayDeque<>();
        this.hotelNames = new HashMap<>();
        this.hotelInfo = new HashMap<>();
        this.holdingsValue = new HashMap<>();
        this.purchasePrices = new HashMap<>();
        this.entrancesPrice = new HashMap<>();
        this.basicBuildingsCost = new HashMap<>();
        this.dailyStayPrices = new HashMap<>();
        this.upgradesCost = new HashMap<>();
        this.upgradedDailyStayPrices = new HashMap<>();
        this.outdoorPremisesCost = new HashMap<>();
        this.finalDailyStayPrices = new HashMap<>();
        this.holdingOwnedBy = new HashMap<>();
        this.hotelUpgrades = new HashMap<>();
        this.playerOwnings = new ArrayList<>();
        ArrayList<Integer> p1 = new ArrayList<>();
        ArrayList<Integer> p2 = new ArrayList<>();
        ArrayList<Integer> p3 = new ArrayList<>();
        this.playerOwnings.add(p1);
        this.playerOwnings.add(p2);
        this.playerOwnings.add(p3);
        this.entranceAdjacentHotels = new HashMap<>();
        this.hotelEntrances = new HashMap<>();
        this.boughtEntrances = new HashMap<>();
        this.boughtEntranceForHotel = new HashMap<>();
        this.playerEntrances = new HashMap<>();
        this.playerEntrances.put(0,new ArrayList<>());
        this.playerEntrances.put(1,new ArrayList<>());
        this.playerEntrances.put(2,new ArrayList<>());
    }

    public String getPlayerName(int player){ return players[player].getPlayerName(); }

    public Label getPlayerNameLabel(int player) { return players[player].getPlayerNameLabel(); }

    public int getPlayerMoney(int player){ return players[player].getPlayerMoney(); }

    public  void setPlayerMoney(int player, int money) {
        int peakMoney = this.players[player].getPlayerPeakMoney();
        if(money > peakMoney) this.players[player].setPlayerPeakMoney(money);
        this.players[player].setPlayerMoney(money);
    }

    public Label getPlayerMoneyLabel(int player) { return players[player].getPlayerMoneyLabel(); }

    public int getPlayerPeakMoney(int player){ return players[player].getPlayerPeakMoney(); }

    public void setPlayerRow(int player, int row){ this.players[player].setPlayerRow(row);}

    public int getPlayerRow(int player){ return this.players[player].getPlayerRow();}

    public void setPlayerCol(int player, int col){ this.players[player].setPlayerCol(col);}

    public int getPlayerCol(int player){ return this.players[player].getPlayerCol();}

    public String getPlayerColor(int player) { return players[player].getColor();}

    public Circle getPlayerAvatar(int player) { return this.players[player].getPlayerAvatar(); }

    public void setStartingPosition(int x, int y) {
        this.startRow = x;
        this.startCol = y;
        this.setPlayerRow(0, x);
        this.setPlayerRow(1, x);
        this.setPlayerRow(2, x);

        this.setPlayerCol(0, y);
        this.setPlayerCol(1, y);
        this.setPlayerCol(2, y);
    }

    public int getStartRow(){ return this.startRow; }

    public int getStartCol(){ return this.startCol; }

    public int getAvailableHotels() { return this.availableHotels; }

    public Label getAvailableHotelsLabel() { return this.availableHotelsLabel; }

    public void setAvailableHotels(int hotelsNum) {
        if(hotelsNum>=0 && hotelsNum<=this.hotelNames.size()) {
            this.availableHotels = hotelsNum;
            this.availableHotelsLabel.setText(Integer.toString(hotelsNum));
        }
    }

    public TitledPane getHotelInfo(int hotelID) { return this.hotelInfo.get(hotelID); }

    public GridPane getGrid() { return this.grid; }

    public void readMap(String path){
        GridPane g = new GridPane();
        g.setGridLinesVisible(true);
        try {
            File file = new File(path + "/board.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s;
            String[] parts;
            int i = 0;
            while ((s = br.readLine()) != null) {
                parts = s.split(",");
                for (int j = 0; j < parts.length; j++) {
                    parts[j].replaceAll("\\s+", ""); // remove all whitespaces so that it resembles a character
                    this.map[i][j] = parts[j];
                    if (parts[j].equals("E") || parts[j].equals("S") || parts[j].equals("B") || parts[j].equals("H") || parts[j].equals("F") || parts[j].equals("C")) {
                        if (parts[j].equals("S")) {
                            this.setStartingPosition(i, j);
                        }
                        Label label = new Label(parts[j]);
                        label.setAlignment(Pos.CENTER);
                        if(parts[j].equals("F")){
                            label.setTextFill(Paint.valueOf("#F2C12E"));
                            Paint color = Paint.valueOf("#5A514C");
                            BackgroundFill bf = new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY);
                            VBox vbox = new VBox();
                            vbox.setBackground(new Background(bf));
                            vbox.setPrefHeight(30.0f);
                            vbox.setPrefWidth(100.0f);
                            g.add(vbox, j, i);
                        }
                        this.gridElements[i][j] = label;
                        g.add(label, j, i);
                        g.setHalignment(label, HPos.CENTER);
                    } else {
                        int number = Integer.parseInt(parts[j]);
                        Paint color = Paint.valueOf(colors.getColor(number));
                        BackgroundFill bf = new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY);
                        VBox vb = new VBox();
                        vb.setBackground(new Background(bf));
                        vb.setPrefHeight(30.0f);
                        vb.setPrefWidth(100.0f);
                        this.gridElements[i][j] = vb;
                        g.add(vb, j, i);

                        if (this.hotels.isEmpty() || !this.hotels.contains(number)) {
                            this.hotels.push(number);
                        }
                    }
                }
                i++;
            }
            this.findStartingDirection();
            this.calculatingPlayingOrder();
            this.grid = g;
            this.setEntrances();
        }
        catch(Exception e){
            System.out.println("Something went wrong when reading game's map.");
            System.exit(1);
        }
    }

    private void setEntrances() {
        Deque<Integer> nearbyHotels;
        for(int i=0;i<12;i++){
            for(int j=0;j<15;j++){
                if(this.map[i][j].charAt(0) == 'E') {
                    Pair<Integer,Integer> pair = new Pair<>(i,j);
                    nearbyHotels = getNearbyHotels(i, j);
                    ArrayList<Integer> hotels = new ArrayList<>();
                    for (Integer hotelID: nearbyHotels) {
                        hotels.add(hotelID);
                        ArrayList<Pair<Integer,Integer>> ar = hotelEntrances.get(hotelID);
                        if(ar == null){
                            ar = new ArrayList<>();
                            ar.add(pair);
                            hotelEntrances.put(hotelID,ar);
                        }
                        else {
                            ar.add(pair);
                        }
                    }
                    entranceAdjacentHotels.put(pair, hotels);
                    boughtEntrances.put(pair,-1); // -1 notifies that no player has bought this entrance.
                    boughtEntranceForHotel.put(pair,-1); // -1 notifies that thi entrance is not assigned to any hotel.
                }
            }
        }
    }

    public void readHotelInfo(String path){
        Iterator<Integer> iterator = this.hotels.iterator();
        while(iterator.hasNext()){
            int hotelID = iterator.next();
            String pathToHotelInfo = path+"/"+hotelID+".txt";
            File hotelInfoFile = new File(pathToHotelInfo);
            try {
                BufferedReader br = new BufferedReader(new FileReader(hotelInfoFile));
                String str = br.readLine();
                this.hotelNames.put(hotelID, str);
                TitledPane titledpane = new TitledPane();
                titledpane.setText(this.hotelNames.get(hotelID));
                String [] s = br.readLine().split(",");
                holdingsValue.put(hotelID,Integer.valueOf(s[0]));
                purchasePrices.put(hotelID,Integer.valueOf(s[1]));
                str = br.readLine();
                entrancesPrice.put(hotelID, Integer.valueOf(str));
                str = br.readLine();
                s = str.split(",");
                basicBuildingsCost.put(hotelID,Integer.valueOf(s[0]));
                dailyStayPrices.put(hotelID,Integer.valueOf(s[1]));
                String curr = br.readLine();
                ArrayList<Integer> upgradesCostList = new ArrayList<>();
                ArrayList<Integer> upgradedDailyStayPricesList = new ArrayList<>();
                while((str = br.readLine()) != null){
                    s = curr.split(",");
                    upgradesCostList.add(Integer.valueOf(s[0]));
                    upgradedDailyStayPricesList.add(Integer.valueOf(s[1]));
                    curr = str;

                }
                upgradesCost.put(hotelID, upgradesCostList);
                upgradedDailyStayPrices.put(hotelID, upgradedDailyStayPricesList);
                s = curr.split(",");
                outdoorPremisesCost.put(hotelID, Integer.valueOf(s[0]));
                finalDailyStayPrices.put(hotelID, Integer.valueOf(s[1]));
                holdingOwnedBy.put(hotelID, -1); // -1 notifies that holding isn't owned by anyone.
                hotelUpgrades.put(hotelID, -1); // -1 notifies that hotel is not upgraded. 0 for basic building & >0 for any other upgrade.
                GridPane info = new GridPane();
                int row =0;
                info.add(new Label("Value of holding: "+holdingsValue.get(hotelID)),0,row);
                info.add(new Label("Purchase price: "+purchasePrices.get(hotelID)),2,row);
                row++;
                info.add(new Label("Entrance price: "+entrancesPrice.get(hotelID)),0,row);
                row++;
                info.add(new Label("Basic building cost: "+basicBuildingsCost.get(hotelID)),0,row);
                info.add(new Label("Stay per day price: "+dailyStayPrices.get(hotelID)),2,row);
                row++;
                int count = 1;
                Iterator<Integer> iterateUpgradesCost = upgradesCost.get(hotelID).iterator();
                Iterator<Integer> iterateUpgradedDailyStayPrices = upgradedDailyStayPrices.get(hotelID).iterator();
                while(iterateUpgradesCost.hasNext()){
                    str = "Upgrade cost #"+String.valueOf(count)+": "+String.valueOf(iterateUpgradesCost.next());
                    info.add(new Label(str),0,row);
                    str = "Stay per day price: "+String.valueOf(iterateUpgradedDailyStayPrices.next());
                    info.add(new Label(str),2,row);
                    count++;
                    row++;
                }
                info.add(new Label("Outdoor premises cost: "+outdoorPremisesCost.get(hotelID)),0,row);
                info.add(new Label("Final per day stay price: "+finalDailyStayPrices.get(hotelID)),2,row);
                titledpane.setContent(info);
                this.hotelInfo.put(hotelID, titledpane);
            }
            catch(Exception e){
                System.out.println("File reading error when reading hotel info.");
                System.exit(1);

            }
        }
    }

    public boolean isPathTile(int row, int col){
        return (this.map[row][col].charAt(0) == 'E' || this.map[row][col].charAt(0) == 'H' || this.map[row][col].charAt(0) == 'S' || this.map[row][col].charAt(0) == 'B' || this.map[row][col].charAt(0) == 'C');
    }

    public boolean isHorizontalLine(int row, int col){ return (isPathTile(row-1, col) && isPathTile(row+1, col)); }
    public boolean isVerticalLine(int row, int col){ return (isPathTile(row, col-1) && isPathTile(row, col+1)); }
    public boolean isTopLeftCorner(int row, int col){ return (isPathTile(row+1, col) && isPathTile(row, col+1)); }
    public boolean isTopRightCorner(int row, int col){ return (isPathTile(row, col-1) && isPathTile(row+1, col)); }
    public boolean isBottomRightCorner(int row, int col){ return (isPathTile(row-1, col) && isPathTile(row, col-1)); }
    public boolean isBottomLeftCorner(int row, int col){ return (isPathTile(row-1, col) && isPathTile(row, col+1)); }

    public int [] move(int initialRow, int initialCol) {

        int [] newPosition = new int[2];
        //is it a horizontal line?
        if(isHorizontalLine(initialRow, initialCol)){
            if(direction[whoPlaysIndex] == Direction.downwards){
                newPosition[0] = initialRow+1;
                newPosition[1] = initialCol;
            }
            else if(direction[whoPlaysIndex] == Direction.upwards){
                newPosition[0] = initialRow-1;
                newPosition[1] = initialCol;
            }
        }

        //is it a vertical line?
        else if(isVerticalLine(initialRow, initialCol)){
            if(direction[whoPlaysIndex] == Direction.rightwards){
                newPosition[0] = initialRow;
                newPosition[1] = initialCol+1;
            }
            else if(direction[whoPlaysIndex] == Direction.leftwards){
                newPosition[0] = initialRow;
                newPosition[1] = initialCol-1;
            }
        }

        //is it a ⌜(top-left corner)?
        else if(isTopLeftCorner(initialRow, initialCol)){
            if(direction[whoPlaysIndex] == Direction.upwards){
                direction[whoPlaysIndex] = Direction.rightwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol+1;
            }
            else if(direction[whoPlaysIndex] == Direction.leftwards){
                direction[whoPlaysIndex] = Direction.downwards;
                newPosition[0] = initialRow+1;
                newPosition[1] = initialCol;
            }
        }

        //is it a ⌝(top-right corner)?
        else if(isTopRightCorner(initialRow, initialCol)){
            if(direction[whoPlaysIndex] == Direction.upwards){
                direction[whoPlaysIndex] = Direction.leftwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol-1;
            }
            else if(direction[whoPlaysIndex] == Direction.rightwards || direction[whoPlaysIndex] == Direction.downwards){
                direction[whoPlaysIndex] = Direction.downwards;
                newPosition[0] = initialRow+1;
                newPosition[1] = initialCol;
            }
        }

        //is it a ⌟ (bottom-right corner)?
        else if(isBottomRightCorner(initialRow, initialCol)){
            if(direction[whoPlaysIndex] == Direction.downwards){
                direction[whoPlaysIndex] = Direction.leftwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol-1;
            }
            else if(direction[whoPlaysIndex] == Direction.rightwards || direction[whoPlaysIndex] == Direction.upwards){
                direction[whoPlaysIndex] = Direction.upwards;
                newPosition[0] = initialRow-1;
                newPosition[1] = initialCol;
            }
        }

        //is it a ⌞(bottom-left corner)?
        else if(isBottomLeftCorner(initialRow, initialCol)) {
            if(direction[whoPlaysIndex] == Direction.downwards){
                direction[whoPlaysIndex] = Direction.rightwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol+1;
            }
            else if(direction[whoPlaysIndex] == Direction.leftwards){
                direction[whoPlaysIndex] = Direction.upwards;
                newPosition[0] = initialRow-1;
                newPosition[1] = initialCol;
            }
        }

        return newPosition;

    }

    public void findStartingDirection() {

        if(isHorizontalLine(this.startRow, this.startCol)) {
            if(this.startCol>7){
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.downwards;
                }
            }
            else{
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.upwards;
                }
            }
        }
        else if(isVerticalLine(this.startRow, this.startCol)){
            if(this.startRow>7){
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.leftwards;
                }
            }
            else{
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.rightwards;
                }
            }
        }
        else if(isTopLeftCorner(this.startRow, this.startCol)){
            for(int i=0; i<3; i++) {
                this.direction[i] = Direction.rightwards;
            }
        }
        else if(isTopRightCorner(this.startRow, this.startCol)){
            if(this.startRow>7){
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.leftwards;
                }
            }
            else{
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.downwards;
                }
            }
        }
        else if(isBottomRightCorner(this.startRow, this.startCol)){
            if(this.startRow>7){
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.leftwards;
                }
            }
            else{
                for(int i=0; i<3; i++) {
                    this.direction[i] = Direction.upwards;
                }
            }
        }
        else if(isBottomLeftCorner(this.startRow, this.startCol)){
            for(int i=0; i<3; i++) {
                this.direction[i] = Direction.upwards;
            }
        }
    }

    public void calculatingPlayingOrder() {

        ArrayList<Integer> lottery = new ArrayList<>();
        lottery.add(0);
        lottery.add(1);
        lottery.add(2);
        int howManyLeft = 3;
        int ordered = 0;
        while(ordered < 3) {
            Double randomNumber = Math.floor(howManyLeft * Math.random());
            int whoGoes = randomNumber.intValue();
            this.playingOrder[ordered] = lottery.get(whoGoes);
            this.initialOrder[this.playingOrder[ordered]] = ordered;
            lottery.remove(whoGoes);
            howManyLeft--;
            ordered++;
        }
        this.whoPlaysIndex = 0; // start with player inserted first in the playingOrder array.
        Player [] p = new Player[3];
        for(int i=0; i<3; i++){
            p[i] = this.players[this.playingOrder[i]];
        }
        this.players = p; // sort players by their new order.
    }

    public int getWhoPlaysIndex(){ return whoPlaysIndex; }

    public int getPlayingOrder(int i){ return playingOrder[i]; }

    public int getInitialOrder(int i){ return initialOrder[i]; }

    public int nextPlayer() {
        int next = (this.whoPlaysIndex+1)%3;
        while(!this.isActivePlayer[next]) next = (next+1)%3;
        this.whoPlaysIndex = next;
        String text = "|| "+players[whoPlaysIndex].getPlayerName()+" is playing now ||";
        String enclose = "";
        for(int i=0;i<text.length();i++){
            enclose = enclose+"=";
        }
        System.out.println("\n"+enclose);
        System.out.println(text);
        System.out.println(enclose);
        return whoPlaysIndex;
    }

    public void checkEligibilityForAllowance() {
        int row = getPlayerRow(whoPlaysIndex);
        int col = getPlayerCol(whoPlaysIndex);
        if(this.map[row][col].equals("B")) players[whoPlaysIndex].setPlayerMoneyRequestEligibility(true);
    }

    public void checkEligibilityForEntrance() {
        int row = getPlayerRow(whoPlaysIndex);
        int col = getPlayerCol(whoPlaysIndex);
        if(this.map[row][col].equals("C")) players[whoPlaysIndex].setPlayerBuildingEligibility(true);
    }

    public boolean getPlayerMoneyRequestEligibility(int player){
        return players[player].getPlayerMoneyRequestEligibility();
    }

    public void setPlayerMoneyRequestEligibility(int player, boolean flag){
        players[player].setPlayerMoneyRequestEligibility(flag);
    }

    public boolean getPlayerEntranceEligibility(int player){
        return players[player].getPlayerBuildingEligibility();
    }

    public void setPlayerEntranceEligibility(int player, boolean flag){
        players[player].setPlayerBuildingEligibility(flag);
    }

    public int determineNumOfMoves (int player, int moves) { // collision(if two players land on the same tile) detection

        int i=-1,j=-1;
        switch(player) {
            case (0):
                i=1;
                j=2;
                break;
            case (1):
                i=0;
                j=2;
                break;
            case (2):
                i=0;
                j=1;
                break;
        }
        if(i==-1 || j==-1){
            System.out.println("Player indexing error.Exiting..");
            System.exit(1);
        }
        int [] destinedTile = new int [2];
        destinedTile[0] = players[player].getPlayerRow();
        destinedTile[1] = players[player].getPlayerCol();
        hypotheticalDirection = direction[whoPlaysIndex];
        for(int k=0;k<moves;k++){
            destinedTile = hypotheticalMove(destinedTile[0], destinedTile[1]);
        }
        if(moves == 1){
            if((players[i].getPlayerRow() == destinedTile[0] && players[i].getPlayerCol() == destinedTile[1]) ||
                    (players[j].getPlayerRow() == destinedTile[0] && players[j].getPlayerCol() == destinedTile[1]))
            {
                int [] nextOfDestinedTile = new int [2];
                nextOfDestinedTile[0] = players[player].getPlayerRow();
                nextOfDestinedTile[1] = players[player].getPlayerCol();
                hypotheticalDirection = direction[whoPlaysIndex];
                for(int k=0;k<moves+1;k++){
                    nextOfDestinedTile = hypotheticalMove(nextOfDestinedTile[0], nextOfDestinedTile[1]);
                }
                if((players[i].getPlayerRow() == nextOfDestinedTile[0] && players[i].getPlayerCol() == nextOfDestinedTile[1]) ||
                        (players[j].getPlayerRow() == nextOfDestinedTile[0] && players[j].getPlayerCol() == nextOfDestinedTile[1]))
                {
                    return 3;
                }
                else {
                    return 2;
                }
            }

            return 1;
        }
        int [] previousOfDestinedTile = new int [2];
        previousOfDestinedTile[0] = players[player].getPlayerRow();
        previousOfDestinedTile[1] = players[player].getPlayerCol();
        hypotheticalDirection = direction[whoPlaysIndex];
        for(int k=0;k<moves-1;k++){
            previousOfDestinedTile = hypotheticalMove(previousOfDestinedTile[0], previousOfDestinedTile[1]);
        }
        if((players[i].getPlayerRow() == destinedTile[0] && players[i].getPlayerCol() == destinedTile[1])) { // if another player is in destined tile
            if((players[j].getPlayerRow() == previousOfDestinedTile[0] && players[j].getPlayerCol() == previousOfDestinedTile[1])) { // check if third player is in previous than destine tile
                return moves+1; // then go to the next of the destined tile.
            }
            else {
                return moves-1;
            }
        }
        //we check the same conditions in case the other two players are switched from the previous if case.
        if((players[j].getPlayerRow() == destinedTile[0] && players[j].getPlayerCol() == destinedTile[1])){
            if((players[i].getPlayerRow() == previousOfDestinedTile[0] && players[i].getPlayerCol() == previousOfDestinedTile[1])) {
                return moves+1;
            }
            else {
                return moves-1;
            }
        }
        return moves;
    }

    public int [] hypotheticalMove(int initialRow, int initialCol) {

        int [] newPosition = new int[2];
        //is it a horizontal line?
        if(isHorizontalLine(initialRow, initialCol)){
            if(hypotheticalDirection == Direction.downwards){
                newPosition[0] = initialRow+1;
                newPosition[1] = initialCol;
            }
            else if(hypotheticalDirection == Direction.upwards){
                newPosition[0] = initialRow-1;
                newPosition[1] = initialCol;
            }
        }

        //is it a vertical line?
        else if(isVerticalLine(initialRow, initialCol)){
            if(hypotheticalDirection == Direction.rightwards){
                newPosition[0] = initialRow;
                newPosition[1] = initialCol+1;
            }
            else if(hypotheticalDirection == Direction.leftwards){
                newPosition[0] = initialRow;
                newPosition[1] = initialCol-1;
            }
        }

        //is it a ⌜(top-left corner)?
        else if(isTopLeftCorner(initialRow, initialCol)){
            if(hypotheticalDirection == Direction.upwards){
                hypotheticalDirection = Direction.rightwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol+1;
            }
            else if(hypotheticalDirection == Direction.leftwards){
                hypotheticalDirection = Direction.downwards;
                newPosition[0] = initialRow+1;
                newPosition[1] = initialCol;
            }
        }

        //is it a ⌝(top-right corner)?
        else if(isTopRightCorner(initialRow, initialCol)){
            if(hypotheticalDirection == Direction.upwards){
                hypotheticalDirection = Direction.leftwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol-1;
            }
            else if(hypotheticalDirection == Direction.rightwards || hypotheticalDirection == Direction.downwards){
                hypotheticalDirection = Direction.downwards;
                newPosition[0] = initialRow+1;
                newPosition[1] = initialCol;
            }
        }

        //is it a ⌟ (bottom-right corner)?
        else if(isBottomRightCorner(initialRow, initialCol)){
            if(hypotheticalDirection == Direction.downwards){
                hypotheticalDirection = Direction.leftwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol-1;
            }
            else if(hypotheticalDirection == Direction.rightwards || hypotheticalDirection == Direction.upwards){
                hypotheticalDirection = Direction.upwards;
                newPosition[0] = initialRow-1;
                newPosition[1] = initialCol;
            }
        }

        //is it a ⌞(bottom-left corner)?
        else if(isBottomLeftCorner(initialRow, initialCol)) {
            if(hypotheticalDirection == Direction.downwards){
                hypotheticalDirection = Direction.rightwards;
                newPosition[0] = initialRow;
                newPosition[1] = initialCol+1;
            }
            else if(hypotheticalDirection == Direction.leftwards){
                hypotheticalDirection = Direction.upwards;
                newPosition[0] = initialRow-1;
                newPosition[1] = initialCol;
            }
        }

        return newPosition;

    }

    public void setPlayerInactiveBackground (int player){
        this.players[player].notPlayerTurn();
    }

    public void setPlayerActiveBackground (int player){
        this.players[player].playerTurn();
    }

    public boolean isHTile(int player){
        int row = players[player].getPlayerRow();
        int col = players[player].getPlayerCol();
        return (this.map[row][col].equals("H"));
    }

    public boolean isETile(int player){
        int row = players[player].getPlayerRow();
        int col = players[player].getPlayerCol();
        return (this.map[row][col].equals("E"));
    }

    public Deque<Integer> getNearbyHotels(int row, int col) {
        int hotelID;
        Deque<Integer> nearbyHotels = new ArrayDeque<>();
        ArrayList<Integer> array = new ArrayList<>();
        if(Character.isDigit(this.map[row-1][col].charAt(0))){ // north
            hotelID =Integer.parseInt(this.map[row - 1][col]);
            boolean found = false;
            for (int i: nearbyHotels) {
                if(i == hotelID)
                    found = true;
            }
            if(!found)
                nearbyHotels.add(hotelID);
        }
        if(Character.isDigit(this.map[row+1][col].charAt(0))) { // south
            hotelID = Integer.parseInt(this.map[row + 1][col]);
            boolean found = false;
            for (int i: nearbyHotels) {
                if(i == hotelID)
                    found = true;
            }
            if(!found)
                nearbyHotels.add(hotelID);
        }
        if(Character.isDigit(this.map[row][col-1].charAt(0))) { // west
            hotelID = Integer.parseInt(this.map[row][col - 1]);
            boolean found = false;
            for (int i: nearbyHotels) {
                if(i == hotelID)
                    found = true;
            }
            if(!found)
                nearbyHotels.add(hotelID);
        }
        if(Character.isDigit(this.map[row][col+1].charAt(0))) { // north
            hotelID = Integer.parseInt(this.map[row][col + 1]);
            boolean found = false;
            for (int i: nearbyHotels) {
                if(i == hotelID)
                    found = true;
            }
            if(!found)
                nearbyHotels.add(hotelID);
        }
        return nearbyHotels;
    }

    public String getHotelName(int hotelID) {
        return hotelNames.get(hotelID);
    }

    public int getHoldingValue(int hotelID) {
        return holdingsValue.get(hotelID);
    }

    public int getPurchasePrice(int hotelID) {
        return purchasePrices.get(hotelID);
    }

    public int getHoldingOwnedBy(int hotelID) {
        return holdingOwnedBy.get(hotelID);
    }

    public HashMap<Integer, Integer> getHotelsOwner() {
        return holdingOwnedBy;
    }

    public void setHoldingOwnedBy(int hotelID, int player) {
        int temp = this.holdingOwnedBy.get(hotelID);
        this.holdingOwnedBy.replace(hotelID, player);
    }

    public int getHotelUpgrades(int hotelID) {
        return hotelUpgrades.get(hotelID);
    }

    public void setHotelUpgrades(int hoteID, int hotelUpgrade) {
        this.hotelUpgrades.replace(hoteID, hotelUpgrade) ;
    }

    public ArrayList<Integer> getPlayerOwnings(int player) {
        return playerOwnings.get(player);
    }

    public void addPlayerOwnings(int player, int hotelID) {
        this.playerOwnings.get(player).add(hotelID);
    }

    public void transferPlayerOwnings(int oldPlayer, int newPlayer, int hotelID) {
//        this.playerOwnings.get(oldPlayer).remove(hotelID);
        this.playerOwnings.get(oldPlayer).remove(Integer.valueOf(hotelID));
        this.hotelUpgrades.replace(hotelID, -1);
        this.playerOwnings.get(newPlayer).add(hotelID);
    }

    public void removePlayerOwnings(int player, int hotelID) {
        this.playerOwnings.get(player).remove(hotelID);
        this.hotelUpgrades.replace(hotelID, -1);
    }

    public void removeAllPlayerOwnings(int player) {
        this.playerOwnings.get(player).clear();
    }

    public int getDailyStayPrice(int hotelID) {
        return dailyStayPrices.get(hotelID);
    }

    public int getUpgradedDailyStayPrice(int hotelID, int upgradeLevel) {
        return upgradedDailyStayPrices.get(hotelID).get(upgradeLevel);
    }

    public int getFinalDailyStayPrice(int hotelID) {
        return finalDailyStayPrices.get(hotelID);
    }

    public int getNumberOfUpgrades(int hotelID) {
        return this.upgradesCost.get(hotelID).size();
    }

    public ArrayList<Pair<Integer, Integer>> getHotelEntrances(int hotelID) {
        return hotelEntrances.get(hotelID);
    }

    public int getBoughtEntrances(Pair<Integer,Integer> entrance) {
        return boughtEntrances.get(entrance);
    }

    public void setBoughtEntrances(Pair<Integer,Integer> entrance, int player) {
        this.boughtEntrances.replace(entrance, player);
    }

    public int getBoughtEntranceForHotel(Pair<Integer,Integer> entrance) {
        return boughtEntranceForHotel.get(entrance);
    }

    public void setBoughtEntranceForHotel(Pair<Integer,Integer> entrance, int hotelID) {
        this.boughtEntranceForHotel.replace(entrance, hotelID);
    }

    public void addPlayerEntrance(int player, Pair<Integer,Integer> entrance) {
        this.playerEntrances.get(player).add(entrance);
    }

    public int getActivePlayers() {
        return this.activePlayers;
    }

    public int getUpgradeCost(int hotelID, int currUpgradeLevel){
        int numOfUpgrades = getNumberOfUpgrades(hotelID);
        if(currUpgradeLevel == -1 ){
            return basicBuildingsCost.get(hotelID);
        }
        else if(currUpgradeLevel == numOfUpgrades+1){
            return -1;
        }
        else if(currUpgradeLevel == numOfUpgrades){  // next upgrade == outdoor premises
            return outdoorPremisesCost.get(hotelID);
        }
        else
            return upgradesCost.get(hotelID).get(currUpgradeLevel);
    }

    public boolean isFinallyUpgradable(int hotelID){
        int currLevel = hotelUpgrades.get(hotelID);
        if(currLevel == upgradesCost.get(hotelID).size())
            return true;
        else
            return false;
    }

    public int getEntrancesPrice(int hotelID) {
        return entrancesPrice.get(hotelID);
    }

    public ArrayList<Pair<Integer, Integer>> getPlayerEntrances(int player) {
        return playerEntrances.get(player);
    }

    public Pair<Integer,Integer> geAvailableEntrance(int hotelID) {
        Pair<Integer,Integer> pair = null;
        ArrayList<Pair<Integer,Integer>> list = this.hotelEntrances.get(hotelID);
        for (Pair<Integer,Integer> p : list) {
            if(this.boughtEntrances.get(p) == -1){ // this entrance is available
                pair = p;
                break;
            }
        }
        return pair;
    }

    public void changeTileColor(Pair<Integer,Integer> tile, int player){
        int row = tile.getKey();
        int col = tile.getValue();
        String playerColor = players[player].getColor();
        if(!Character.isDigit(this.map[row][col].charAt(0))){ // we are not interested in hotel tiles.
            Label label = (Label) this.gridElements[row][col];
            label.setTextFill(Paint.valueOf(playerColor));
        }
    }

    public void restoreTileColor(Pair<Integer,Integer> tile){
        int row = tile.getKey();
        int col = tile.getValue();
        String color = "#000000";
        if(!Character.isDigit(this.map[row][col].charAt(0))){ // we are not interested in hotel tiles.
            Label label = (Label) this.gridElements[row][col];
            label.setTextFill(Paint.valueOf(color));
        }
    }

    public void playerIsOut(int player) {
        this.isActivePlayer[player] = false;
        this.activePlayers--;

        ArrayList<Integer> hotelsToBeFreed = this.playerOwnings.get(player);
        this.availableHotels = this.availableHotels + hotelsToBeFreed.size();
        this.availableHotelsLabel.setText(Integer.toString(availableHotels));
        for (int hotelID : hotelsToBeFreed ) {
            hotelUpgrades.replace(hotelID, -1);
            holdingOwnedBy.replace(hotelID,-1);
        }
        for (Pair<Integer,Integer> entrance: playerEntrances.get(player)) {
            boughtEntrances.replace(entrance,-1);
            boughtEntranceForHotel.replace(entrance,-1);
            restoreTileColor(entrance);
        }
        this.playerOwnings.get(player).clear();
    }

    public void freeHotelEntrancesFromPlayer(int hotelID, int player) {
        Iterator<Pair<Integer,Integer>> entrances = playerEntrances.get(player).iterator();
        while(entrances.hasNext()){
            Pair<Integer,Integer> entrance = entrances.next();
            if (boughtEntranceForHotel.get(entrance) == hotelID) {
                boughtEntrances.replace(entrance,-1);
                boughtEntranceForHotel.replace(entrance,-1);
                restoreTileColor(entrance);
                entrances.remove();
            }
        }
    }

    public void freeHotelFromPlayer(int hotelID, int player) {
        setAvailableHotels(this.availableHotels+1);
        holdingOwnedBy.replace(hotelID,-1);
        Iterator<Pair<Integer,Integer>> entrances = playerEntrances.get(player).iterator();
        while(entrances.hasNext()){
            Pair<Integer,Integer> entrance = entrances.next();
            if (boughtEntranceForHotel.get(entrance) == hotelID) {
                boughtEntrances.replace(entrance,-1);
                boughtEntranceForHotel.replace(entrance,-1);
                restoreTileColor(entrance);
                entrances.remove();
            }
        }
    }

    public void setPlayerOffBackground (int player) {
        this.players[player].PlayerIsOff();
    }
}
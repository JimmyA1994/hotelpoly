package HotelTyphoon;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import java.io.File;
import java.util.*;
import javafx.application.Platform;
import javafx.util.Pair;

public class Controller {

    private Game game;

    private Timeline timeline;
    private int secs = 0;
    private int mins = 0;

    private boolean buttonWasPressed;

    @FXML
    private GridPane infoGrid;

    @FXML
    private Label totalTime;

    @FXML
    private BorderPane mainPane;

    @FXML
    private Button rollDiceButton;

    @FXML
    private Button requestBuildingButton;

    @FXML
    private Button buyHotelButton;

    @FXML
    private Button buyEntranceButton;

    @FXML
    private Button requestMoneyButton;

    @FXML
    private void newGame(ActionEvent event) {
        this.timeline.stop();
        totalTime.setText("00:00");
        this.secs = 0;
        this.mins = 0;
        infoGrid.getChildren().remove(game.getPlayerNameLabel(0));
        infoGrid.getChildren().remove(game.getPlayerNameLabel(1));
        infoGrid.getChildren().remove(game.getPlayerNameLabel(2));
        infoGrid.getChildren().remove(game.getPlayerMoneyLabel(0));
        infoGrid.getChildren().remove(game.getPlayerMoneyLabel(1));
        infoGrid.getChildren().remove(game.getPlayerMoneyLabel(2));
        infoGrid.getChildren().remove(game.getAvailableHotelsLabel());
        System.out.println("A new game was started.");
        startGame();
    }

    @FXML
    private void stopGame(ActionEvent event) {
        timeline.pause();
        rollDiceButton.setDisable(true);
        requestBuildingButton.setDisable(true);
        buyHotelButton.setDisable(true);
        buyEntranceButton.setDisable(true);
        requestMoneyButton.setDisable(true);
        System.out.println("Game was stopped.");
    }

    @FXML
    private void displayCards(ActionEvent event){
        Stage popup = new Stage();
        popup.setTitle("Hotel Information");
        Accordion rootNode = new Accordion();
        for (Integer hotelID:game.hotels) {
            rootNode.getPanes().add(game.getHotelInfo(hotelID));

        }
        popup.setScene(new Scene(rootNode, 500, 350));
        // set next as a comment in case we want to interact with primary stage while the popup is up.
        //popup.initModality(Modality.APPLICATION_MODAL);
        popup.show();
    }

    @FXML
    private void closeApplication(ActionEvent event){
        Platform.exit();
        System.out.println("Game was exited.");
    }

    @FXML
    private void hotelsStatistics(ActionEvent event) {
        int count = 0;
        VBox layout = new VBox();
        Label label = new Label("");
        Stage popup = new Stage();
        popup.setTitle("Hotels Statistics");
        HashMap<Integer,Integer> hotelsOwner = game.getHotelsOwner();
        Set set = hotelsOwner.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry me = (Map.Entry) iterator.next();
            int hotelID = (int) me.getKey();
            String hotelName = game.getHotelName(hotelID);
            int owner = (int) me.getValue();
            if (owner == -1) {
                label = new Label(hotelName);
                label.setTextFill(Paint.valueOf(game.colors.getColor(hotelID)));
                layout.getChildren().add(label);
                layout.setMargin(label,new Insets(5,0,0,5));
            }
            else {
                String ownerName = game.getPlayerName(owner);
                label = new Label(hotelName);
                label.setTextFill(Paint.valueOf(game.colors.getColor(hotelID)));
                GridPane g = new GridPane();
                g.add(label,0,0);
                g.add(new Label(" owned by "),1,0);
                Label l = new Label(ownerName);
                l.setTextFill(Paint.valueOf(game.getPlayerColor(owner)));
                g.add(l,2,0);
                layout.getChildren().add(g);
                layout.setMargin(g,new Insets(5,0,0,5));
            }


            int currentUpgradeLevel = game.getHotelUpgrades(hotelID);
            int numOfUpgrades = game.getNumberOfUpgrades(hotelID);
            if (currentUpgradeLevel == -1) {
                label = new Label("Hotel has no upgrades (0/"+(numOfUpgrades+2)+").");
            }
            else if (currentUpgradeLevel == 0) {
                label = new Label("Hotel has the basic building (1/"+(numOfUpgrades+2)+").");
            }
            else if(currentUpgradeLevel == numOfUpgrades + 1) {
                label = new Label("Hotel has been fully upgraded ("+(numOfUpgrades+2)+"/"+(numOfUpgrades+2)+").");
            }
            else {
                String place="";
                if (currentUpgradeLevel == 1) {
                    place="st";
                } else if (currentUpgradeLevel == 2) {
                    place = "nd";
                } else if (currentUpgradeLevel == 3) {
                    place = "rd";
                } else {
                    place="th";
                }
                label = new Label("Hotel has the "+currentUpgradeLevel+place+" upgrade ("+(currentUpgradeLevel+1)+"/"+(numOfUpgrades+2)+").");
            }
            layout.getChildren().add(label);
            layout.setMargin(label,new Insets(5,0,0,10));

            label = new Label("Maximum number of possible upgrades is "+(numOfUpgrades+2)+".");
            layout.getChildren().add(label);
            layout.setMargin(label,new Insets(0,0,0,10));

            count+=3;
        }
        layout.setAlignment(Pos.BASELINE_LEFT);

        Button closeButton = new Button("Close");
        closeButton.setOnAction((e)->popup.close());

        GridPane buttonGrid =new GridPane();
        ColumnConstraints constraint = new ColumnConstraints();
        constraint.setPercentWidth(75);
        buttonGrid.getColumnConstraints().add(constraint);
        constraint = new ColumnConstraints();
        constraint.setPercentWidth(25);
        buttonGrid.getColumnConstraints().add(constraint);

        Region r = new Region();
        r.setMinWidth(10);
        buttonGrid.add(r,0,0);
        buttonGrid.add(closeButton,1,0);
        layout.getChildren().add(buttonGrid);

        int height = 40 + count*20;
        popup.setScene(new Scene(layout, 350, height));
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();
    }

    @FXML
    private void playerEntrances(ActionEvent event) {
        Stage popup = new Stage();
        VBox layout = new VBox();
        Label label;
        int count = 3;
        int width = 0;
        for(int i=0;i<3;i++){
            int player = game.getInitialOrder(i);
            String playerName = game.getPlayerName(player);
            label = new Label(playerName);
            label.setTextFill(Paint.valueOf(game.getPlayerColor(player)));
            layout.getChildren().add(label);
            layout.setMargin(label,new Insets(5,0,0,5));

            ArrayList<Pair<Integer,Integer>> playerEntrances = game.getPlayerEntrances(player);
            if (playerEntrances.isEmpty()) {
                count++;
                String text = "Player owns no entrances.";
                if(width < text.length()) width = text.length();
                label = new Label(text);
                layout.getChildren().add(label);
                layout.setMargin(label,new Insets(5,0,0,10));
            }
            else {
                HashMap<Integer,Integer> numberOFEntrancesPerHotelByPlayer = new HashMap<>();
                for(Pair entrance : game.getPlayerEntrances(player)){
                    int hotelID = game.getBoughtEntranceForHotel(entrance);
                    if(numberOFEntrancesPerHotelByPlayer.containsKey(hotelID)){
                        int numberOfEntrances = numberOFEntrancesPerHotelByPlayer.get(hotelID) + 1;
                        numberOFEntrancesPerHotelByPlayer.replace(hotelID, numberOfEntrances);
                    }
                    else {
                        numberOFEntrancesPerHotelByPlayer.put(hotelID,1);
                    }
                }

                Set set = numberOFEntrancesPerHotelByPlayer.entrySet();
                Iterator iter = set.iterator();
                while(iter.hasNext()) {
                    count++;
                    Map.Entry me = (Map.Entry) iter.next();
                    int hotelID = (int) me.getKey();
                    String hotelName = game.getHotelName(hotelID);
                    Label hotelLabel = new Label(hotelName);
                    Label left = new Label("Total owned entrances for ");
                    hotelLabel.setTextFill(Paint.valueOf(game.colors.getColor(hotelID)));
                    int num = (int) me.getValue();
                    Label right = new Label(": "+num+".");
                    String text = left.getText()+hotelLabel.getText()+right.getText();
                    if(width < text.length()) width = text.length();

                    GridPane g = new GridPane();
                    g.add(left,0,0);
                    g.add(hotelLabel,1,0);
                    g.add(right,2,0);
                    layout.getChildren().add(g);
                    layout.setMargin(g,new Insets(5,0,0,10));
                }
            }
        }
        layout.setAlignment(Pos.BASELINE_LEFT);

        Button closeButton = new Button("Close");
        closeButton.setOnAction((e)->popup.close());

        GridPane buttonGrid =new GridPane();
        ColumnConstraints constraint = new ColumnConstraints();
        constraint.setPercentWidth(75);
        buttonGrid.getColumnConstraints().add(constraint);
        constraint = new ColumnConstraints();
        constraint.setPercentWidth(25);
        buttonGrid.getColumnConstraints().add(constraint);

        Region r = new Region();
        r.setMinWidth(10);
        buttonGrid.add(r,0,0);
        buttonGrid.add(closeButton,1,0);
        layout.getChildren().add(buttonGrid);

        int height = 60 + count * 20;
        width *= 10;
        popup.setTitle("Entrances Statistics");
        popup.setScene(new Scene(layout, width, height));
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();
    }

    @FXML
    private void profits(ActionEvent event) {
        Stage popup = new Stage();
        popup.setTitle("Player Profits");
        Label label;
        VBox layout = new VBox();

        for(int i=0;i<3;i++) {
            int player = game.getInitialOrder(i);
            String playerName = game.getPlayerName(player);
            int playerPeakMoney = game.getPlayerPeakMoney(player);

            GridPane grid = new GridPane();
            label = new Label(playerName);
            label.setTextFill(Paint.valueOf(game.getPlayerColor(player)));
            grid.add(label,0,0);

            label = new Label(" Peak Money: "+playerPeakMoney+".");
            grid.add(label,1,0);
            layout.getChildren().add(grid);
            layout.setMargin(grid, new Insets(0,0,0,30));
        }

        layout.getChildren().add(new Label(""));
        layout.setAlignment(Pos.CENTER);
        Button closeButton = new Button("Close");
        closeButton.setOnAction((e)->popup.close());

        GridPane buttonGrid =new GridPane();
        ColumnConstraints constraint = new ColumnConstraints();
        constraint.setPercentWidth(75);
        buttonGrid.getColumnConstraints().add(constraint);
        constraint = new ColumnConstraints();
        constraint.setPercentWidth(25);
        buttonGrid.getColumnConstraints().add(constraint);

        Region r = new Region();
        r.setMinWidth(10);
        buttonGrid.add(r,0,0);
        buttonGrid.add(closeButton,1,0);
        layout.getChildren().add(buttonGrid);

        popup.setScene(new Scene(layout,250,150));
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();
    }

    @FXML
    private void rollDice(ActionEvent event) {
        int plays = game.getWhoPlaysIndex();
        Double dice = Math.ceil(6*Math.random());
        int times = dice.intValue();
        int diceResult = times;
        System.out.print(game.getPlayerName(game.getWhoPlaysIndex())+" rolled a "+diceResult+" ");
        times = game.determineNumOfMoves(game.getWhoPlaysIndex(), times);
        System.out.println(" but is allowed to move "+times+".");
        for(int i=0; i<times;i++){
            moveOneTile(plays);
        }
        if(game.isHTile(game.getWhoPlaysIndex())) {
            rollDiceButton.setDisable(true);
            buyHotelButton.setDisable(false);
        }
        else if(game.isETile(game.getWhoPlaysIndex())) {
            rollDiceButton.setDisable(true);
            requestBuildingButton.setDisable(false);
            if(game.getPlayerEntranceEligibility(game.getWhoPlaysIndex())){
                buyEntranceButton.setDisable(false);
            }
            boolean hasToPay = PayFee();
            if (hasToPay) {
                advanceTurn();
            }
        }
        else
            advanceTurn();
    }

    @FXML
    private void requestBuilding(ActionEvent event){
        int player = game.getWhoPlaysIndex();
        String playerName = game.getPlayerName(player);
        int playerMoney = game.getPlayerMoney(player);
        Stage popup = new Stage();
        popup.setTitle(playerName+" Request Building");
        Label label;
        int count = 0;
        ArrayList<Integer> playerOwnings = game.getPlayerOwnings(player);
        if(playerOwnings.isEmpty()) {
            displayPlayerHasNoHotels(popup, playerName);
        }
        else {
            BorderPane rootNode = new BorderPane();
            GridPane gridUI = new GridPane();
            rootNode.setCenter(gridUI);
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(25);
            gridUI.getColumnConstraints().add(column);
            column = new ColumnConstraints();
            column.setPercentWidth(75);
            gridUI.getColumnConstraints().add(column);
            label = new Label("Choose between:");
            int leftpart = label.getText().length()+5;
            int width = leftpart;
            int height = 80;
            int maxText = 0;
            gridUI.setHalignment(label, HPos.CENTER);
            gridUI.setValignment(label, VPos.CENTER);
            Label availMoney = new Label("You have available "+playerMoney+" MLs");
            rootNode.setTop(availMoney);
            gridUI.add(label,0, 2);
            ToggleGroup group = new ToggleGroup();
            RadioButton defaultChoice = new RadioButton("I won't upgrade anything");
            defaultChoice.setUserData(0);
            defaultChoice.setToggleGroup(group);
            defaultChoice.setSelected(true);
            count = 1;
            gridUI.add(defaultChoice, 1, count);
            count++;
            String text;
            for (Integer hotelID : playerOwnings) {
                int currHotelLevel = game.getHotelUpgrades(hotelID);
                int upgradeCost = game.getUpgradeCost(hotelID, currHotelLevel);

                if(upgradeCost == -1){
                    text = "No more upgrades left for "+game.getHotelName(hotelID);
                    label = new Label(text);
                    if(text.length() > maxText) {
                        maxText = text.length();
                        width = leftpart + maxText;
                    }
                    gridUI.add(label, 1, count);
                }
                else {
                    RadioButton choice = new RadioButton();
                    choice.setUserData(hotelID);
                    if(game.isFinallyUpgradable(hotelID)){
                        text = "Last upgrade for "+game.getHotelName(hotelID) + ". Request the outdoor premises for " + upgradeCost+" MLs";
                        choice.setText(text);
                        if(text.length() > maxText) {
                            maxText = text.length();
                            width = leftpart + maxText;
                        }
                    }
                    else {
                        if (currHotelLevel == -1) {
                            text = "Request the basic building for "+game.getHotelName(hotelID)+". It'll cost you " + upgradeCost+" MLs";
                            choice.setText(text);
                            if(text.length() > maxText) {
                                maxText = text.length();
                                width = leftpart + maxText;
                            }
                        } else {
                            String place="";
                            if (currHotelLevel == 0) {
                                place="st";
                            } else if (currHotelLevel == 1) {
                                place = "nd";
                            } else if (currHotelLevel == 2) {
                                place = "rd";
                            } else {
                                place="th";
                            }
                            text = "Request the "+(currHotelLevel+1)+place+" upgrade for "+game.getHotelName(hotelID) + ". It'll cost you " + upgradeCost+" ML";
                            choice.setText(text);
                            if(text.length() > maxText) {
                                maxText = text.length();
                                width = leftpart + maxText;
                            }
                        }
                    }
                    gridUI.add(choice, 1, count);
                    choice.setToggleGroup(group);
                    if (upgradeCost > playerMoney) choice.setDisable(true);
                }
                count++;
            }
            gridUI.add(new Label(""),1, count);
            count++;
            Button select = new Button("Select");
            this.buttonWasPressed = false;
            select.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    setButtonWasPressed();
                    popup.close();
                }
            });
            gridUI.add(select, 1, count);
            gridUI.setHalignment(select, HPos.CENTER);

            width*=8;
            height += 20*(count-3);
            popup.setScene(new Scene(rootNode, width, height));
            popup.initModality(Modality.APPLICATION_MODAL);
            popup.showAndWait();

            int playerSelection = Integer.parseInt(group.getSelectedToggle().getUserData().toString());
            if(playerSelection > 0 && this.buttonWasPressed) {
                int currHotelLevel = game.getHotelUpgrades(playerSelection);
                int paymentInFull = game.getUpgradeCost(playerSelection, currHotelLevel);
                int payment = costAfterBuildingCommision(paymentInFull);
                displayCommitteeDecision(playerMoney, paymentInFull, payment);
                if (payment >= 0 && playerMoney > payment) {
                    playerMoney = playerMoney - payment;
                    game.setPlayerMoney(player, playerMoney);
                    game.setHotelUpgrades(playerSelection, currHotelLevel+1);
                }
            }
        }
        advanceTurn();
    }

    @FXML
    private void buyHotel(ActionEvent event){
        int player = game.getWhoPlaysIndex();
        String playerName = game.getPlayerName(player);
        int playerMoney = game.getPlayerMoney(player);
        int count=1;
        Stage popup = new Stage();
        popup.setTitle(playerName+" Purchase Holdings Decision");
        GridPane choiceNode = new GridPane();
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(40);
        choiceNode.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(60);
        choiceNode.getColumnConstraints().add(column);
        Label label = new Label("Choose between:");
        choiceNode.setHalignment(label, HPos.CENTER);
        choiceNode.setValignment(label, VPos.CENTER);
        Label availMoney = new Label("You have available "+playerMoney+" MLs");
        choiceNode.add(availMoney, 0, 0);
        choiceNode.add(label,0, 2);

        int row = game.getPlayerRow(game.getWhoPlaysIndex());
        int col = game.getPlayerCol(game.getWhoPlaysIndex());
        Deque<Integer> nearbyHotels = game.getNearbyHotels(row, col);

        ToggleGroup group = new ToggleGroup();
        RadioButton defaultChoice = new RadioButton("I won't buy anything");
        defaultChoice.setUserData(0);
        defaultChoice.setToggleGroup(group);
        defaultChoice.setSelected(true);
        choiceNode.add(defaultChoice, 1, count);
        count++;
        for (Integer i: nearbyHotels) {
            String hotelName = game.getHotelName(i);
            int hotelsUpgradeLevel = game.getHotelUpgrades(i);
            boolean belongsToSomeoneElse = game.getHoldingOwnedBy(i) != -1;
            boolean canBeBoughtOffOfAnother = belongsToSomeoneElse && hotelsUpgradeLevel == -1;
            if(!belongsToSomeoneElse) {
                int holdingValue = game.getHoldingValue(i);
                RadioButton choice = new RadioButton(hotelName + " valued at " + holdingValue);
                choice.setUserData(i);
                choice.setToggleGroup(group);
                if (holdingValue > playerMoney) choice.setDisable(true);
                choiceNode.add(choice, 1, count);
                count++;
            }
            else if(canBeBoughtOffOfAnother && (game.getHoldingOwnedBy(i) != player) ){
                int purchasePrice = game.getPurchasePrice(i);
                String prevOwner = game.getPlayerName(game.getHoldingOwnedBy(i));
                RadioButton choice = new RadioButton("Buy "+hotelName+" off of "+prevOwner+" for "+purchasePrice);
                choice.setUserData(i);
                choice.setToggleGroup(group);
                if (purchasePrice > playerMoney) choice.setDisable(true);
                choiceNode.add(choice, 1, count);
                count++;
            }
        }

        Button select = new Button("Select");
        this.buttonWasPressed = false;
        select.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                setButtonWasPressed();
                popup.close();
            }
        });
        if (count == 2) {
            VBox root = new VBox();
            root.setAlignment(Pos.CENTER);
            Label l;
            int availableHotels = game.getAvailableHotels();
            if (availableHotels == 0) {
                l = new Label("No hotels available");
            } else {
                l = new Label("No hotels in the nearborhood.");
            }
            Button b = new Button("OK");
            b.setOnAction((e)->popup.close());

            root.getChildren().addAll(l, new Label(""), b);
            popup.setScene(new Scene(root, 300, 80));
        }
        else{
            choiceNode.add(new Label(""),1,count);
            count++;
            choiceNode.add(select, 1, count);
            choiceNode.setHalignment(select, HPos.CENTER);
            int height = 80+20*(count-3);
            popup.setScene(new Scene(choiceNode, 500, height));
        }

        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();

        int playerSelection = Integer.parseInt(group.getSelectedToggle().getUserData().toString());
        if(playerSelection > 0 && this.buttonWasPressed) {
            boolean belongsToSomeoneElse = game.getHoldingOwnedBy(playerSelection) != -1;
            boolean canBeBoughtOffOfAnother = belongsToSomeoneElse && game.getHotelUpgrades(playerSelection) == -1;
            if(!belongsToSomeoneElse) {
                int cost = game.getHoldingValue(playerSelection);
                playerMoney = playerMoney - cost;
                game.setPlayerMoney(player, playerMoney);
                game.setHoldingOwnedBy(playerSelection, player);
                game.addPlayerOwnings(player,playerSelection);
                int availHotels = game.getAvailableHotels();
                game.setAvailableHotels(availHotels-1);
                System.out.println(playerName+" bought "+game.getHotelName(playerSelection)+" for "+cost+" MLs");
            }
            else if(canBeBoughtOffOfAnother) {
                int cost = game.getPurchasePrice(playerSelection);
                int otherPlayer = game.getHoldingOwnedBy(playerSelection);
                int otherPlayerMoney = game.getPlayerMoney(otherPlayer);
                playerMoney = playerMoney - cost;
                otherPlayerMoney = otherPlayerMoney + cost;
                game.setPlayerMoney(player, playerMoney);
                game.setPlayerMoney(otherPlayer, otherPlayerMoney);
                game.setHoldingOwnedBy(playerSelection, player);
                game.transferPlayerOwnings(otherPlayer, player, playerSelection);
                System.out.println(playerName+" bought "+game.getHotelName(playerSelection)+" off of "+game.getPlayerName(otherPlayer)+" for "+cost+" MLs");
            }
        }
        advanceTurn();
        buyHotelButton.setDisable(true);
    }

    @FXML
    private void buyEntrance(ActionEvent event){
        if(game.getPlayerEntranceEligibility(game.getWhoPlaysIndex())) {

            int player = game.getWhoPlaysIndex();
            String playerName = game.getPlayerName(player);
            int playerMoney = game.getPlayerMoney(player);
            Stage popup = new Stage();
            popup.setTitle(playerName+" Buy Entrance");
            Label label;
            int count=0;
            ArrayList<Integer> playerOwnings = game.getPlayerOwnings(player);
            if(playerOwnings.isEmpty()) {
                displayPlayerHasNoHotels(popup, playerName);
            }
            else{
                BorderPane rootNode = new BorderPane();
                GridPane gridUI = new GridPane();
                rootNode.setCenter(gridUI);
                ColumnConstraints column = new ColumnConstraints();
                column.setPercentWidth(25);
                gridUI.getColumnConstraints().add(column);
                column = new ColumnConstraints();
                column.setPercentWidth(75);
                gridUI.getColumnConstraints().add(column);
                label = new Label("Choose between:");
                gridUI.setHalignment(label, HPos.CENTER);
                gridUI.setValignment(label, VPos.CENTER);
                Label availMoney = new Label("You have available "+playerMoney+" MLs");
                rootNode.setTop(availMoney);
                gridUI.add(label,0, 2);
                ToggleGroup group = new ToggleGroup();
                RadioButton defaultChoice = new RadioButton("I won't buy an entrance");
                defaultChoice.setUserData(0);
                defaultChoice.setToggleGroup(group);
                defaultChoice.setSelected(true);
                count = 1;
                gridUI.add(defaultChoice, 1, count);
                count++;
                for (Integer hotelID : playerOwnings) {
                    Pair<Integer,Integer> availableEntrance = game.geAvailableEntrance(hotelID);
                    int EntranceCost = game.getEntrancesPrice(hotelID);

                    if(availableEntrance == null){
                        label = new Label("No more entrances left for "+game.getHotelName(hotelID));
                        gridUI.add(label, 1, count);
                    }
                    else if(game.getHotelUpgrades(hotelID) == -1) {
                        RadioButton choice = new RadioButton();
                        choice.setUserData(hotelID);
                        choice.setText(game.getHotelName(hotelID)+" hasn't been upgraded");
                        gridUI.add(choice, 1, count);
                        choice.setToggleGroup(group);
                        choice.setDisable(true);
                    }
                    else {
                        RadioButton choice = new RadioButton();
                        choice.setUserData(hotelID);
                        choice.setText("Build an entrance for "+game.getHotelName(hotelID) + " for " + EntranceCost);
                        gridUI.add(choice, 1, count);
                        choice.setToggleGroup(group);
                        if (EntranceCost > playerMoney) choice.setDisable(true);
                    }

                    count++;
                }
                Button select = new Button("Select");
                this.buttonWasPressed = false;
                select.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                        setButtonWasPressed();
                        popup.close();
                    }
                });
                gridUI.add(new Label(""), 1, count);
                count++;
                gridUI.add(select, 1, count);
                gridUI.setHalignment(select, HPos.CENTER);

                int height = 20*(count+1);
                popup.setScene(new Scene(rootNode, 550, height));
                popup.initModality(Modality.APPLICATION_MODAL);
                popup.showAndWait();

                int playerSelection = Integer.parseInt(group.getSelectedToggle().getUserData().toString());
                if(playerSelection > 0 && this.buttonWasPressed) {
                    int cost = game.getEntrancesPrice(playerSelection);
                    Pair<Integer,Integer> entrance = game.geAvailableEntrance(playerSelection);
                    playerMoney = playerMoney - cost;
                    game.setPlayerMoney(player, playerMoney);
                    game.setBoughtEntrances(entrance, game.getWhoPlaysIndex());
                    game.setBoughtEntranceForHotel(entrance, playerSelection);
                    game.addPlayerEntrance(player,entrance);
                    game.changeTileColor(entrance, game.getWhoPlaysIndex());
                    System.out.println(playerName+" bought an entrance at "+game.getHotelName(playerSelection)+" for "+cost+" MLs");
                    game.setPlayerEntranceEligibility(game.getWhoPlaysIndex(), false);
                }
            }
            buyEntranceButton.setDisable(true);
            advanceTurn();
        }
    }

    @FXML
    private void requestMoney(ActionEvent event){
        if(game.getPlayerMoneyRequestEligibility(game.getWhoPlaysIndex())) {
            int money = game.getPlayerMoney(game.getWhoPlaysIndex());
            money += 1000;
            game.setPlayerMoney(game.getWhoPlaysIndex(), money);
            game.setPlayerMoneyRequestEligibility(game.getWhoPlaysIndex(), false);
            requestMoneyButton.setDisable(true);
        }
    }

    public void initialize() {
        this.startGame();
    }

    private void startGame() {
        this.game = new Game();

        try {
            String abs_path = new File(".").getCanonicalPath();
            String path_to_boards = abs_path + "/src/HotelTyphoon/resources/boards/";
            File file_of_path = new File(path_to_boards);
            String[] children_of_path = file_of_path.list();
            Stack<String> paths_to_map = new Stack<>();
            for (int i = 0; i < children_of_path.length; i++) {
                File temp_file = new File(path_to_boards + children_of_path[i]);
                if (temp_file.isDirectory()) {
                    paths_to_map.push(path_to_boards + children_of_path[i]);
                }
            }
            double probability = Math.random();
            Double d = Math.floor(probability*paths_to_map.size());
            int chosenMap= d.intValue();
            String chosenPath = paths_to_map.elementAt(chosenMap);

            game.readMap(chosenPath);
            mainPane.setCenter(game.getGrid());
            mainPane.setAlignment(game.getGrid(), Pos.CENTER);

            game.readHotelInfo(chosenPath);
            game.setAvailableHotels(game.hotels.size());
        }
        catch(Exception e){
            System.out.println("Something went wrong checking for map directories.");
            System.exit(1);
        }
        int p1Reference = game.getInitialOrder(0);
        int p2Reference = game.getInitialOrder(1);
        int p3Reference = game.getInitialOrder(2);

        infoGrid.add(game.getPlayerNameLabel(p1Reference),0,0);
        infoGrid.setHalignment(game.getPlayerNameLabel(p1Reference),HPos.RIGHT);
        infoGrid.add(game.getPlayerMoneyLabel(p1Reference),1,0);
        infoGrid.setHalignment(game.getPlayerMoneyLabel(p1Reference),HPos.LEFT);
        infoGrid.add(game.getPlayerNameLabel(p2Reference),2,0);
        infoGrid.setHalignment(game.getPlayerNameLabel(p2Reference),HPos.RIGHT);
        infoGrid.add(game.getPlayerMoneyLabel(p2Reference),3,0);
        infoGrid.setHalignment(game.getPlayerMoneyLabel(p2Reference),HPos.LEFT);
        infoGrid.add(game.getPlayerNameLabel(p3Reference),4,0);
        infoGrid.setHalignment(game.getPlayerNameLabel(p3Reference),HPos.RIGHT);
        infoGrid.add(game.getPlayerMoneyLabel(p3Reference),5,0);
        infoGrid.setHalignment(game.getPlayerMoneyLabel(p3Reference),HPos.LEFT);
        infoGrid.add(game.getAvailableHotelsLabel(), 8, 0);

        game.getGrid().add(game.getPlayerAvatar(p1Reference), game.getStartCol(), game.getStartRow());
        game.getGrid().add(game.getPlayerAvatar(p2Reference), game.getStartCol(), game.getStartRow());
        game.getGrid().add(game.getPlayerAvatar(p3Reference), game.getStartCol(), game.getStartRow());
        game.getGrid().setHalignment(game.getPlayerAvatar(p1Reference), HPos.LEFT);
        game.getGrid().setValignment(game.getPlayerAvatar(p1Reference), VPos.TOP);
        game.getGrid().setHalignment(game.getPlayerAvatar(p2Reference), HPos.RIGHT);
        game.getGrid().setValignment(game.getPlayerAvatar(p2Reference), VPos.TOP);
        game.getGrid().setHalignment(game.getPlayerAvatar(p3Reference), HPos.LEFT);
        game.getGrid().setValignment(game.getPlayerAvatar(p3Reference), VPos.BOTTOM);

        rollDiceButton.setDisable(false);
        requestBuildingButton.setDisable(true);
        buyHotelButton.setDisable(true);
        buyEntranceButton.setDisable(true);
        requestMoneyButton.setDisable(true);

        this.timeline= new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(secs == 60) {
                    mins++;
                    secs = 0;
                }
                totalTime.setText((((mins/10) == 0) ? "0" : "") + mins + ":"
                        + (((secs/10) == 0) ? "0" : "") + secs++);
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);
        timeline.play();

        String text = "|| "+game.getPlayerName(game.getWhoPlaysIndex())+" is playing now ||";
        String enclose = "";
        for(int i=0;i<text.length();i++){
            enclose = enclose+"=";
        }
        System.out.println(enclose);
        System.out.println(text);
        System.out.println(enclose);
        game.setPlayerActiveBackground(game.getWhoPlaysIndex());

    }

    private void moveOneTile(int plays){
        game.getGrid().getChildren().remove(game.getPlayerAvatar(plays));
        int [] newPosition = game.move(game.getPlayerRow(plays),game.getPlayerCol(plays));
        game.setPlayerRow(plays, newPosition[0]);
        game.setPlayerCol(plays, newPosition[1]);
        game.checkEligibilityForAllowance();
        game.checkEligibilityForEntrance();
        game.getGrid().add(game.getPlayerAvatar(plays),game.getPlayerCol(plays), game.getPlayerRow(plays));
    }

    private void advanceTurn(){
        boolean playerSurvived = staysOnGame();
        if(playerSurvived) {
            game.setPlayerInactiveBackground(game.getWhoPlaysIndex());
        }
        else {
            game.setPlayerOffBackground(game.getWhoPlaysIndex());
        }
        game.nextPlayer();
        if(!playerSurvived && game.getActivePlayers() == 1) { // game is over.
            this.timeline.stop();
            String winner = game.getPlayerName(game.getWhoPlaysIndex());
            displayWinner(winner);
        }
        game.setPlayerActiveBackground(game.getWhoPlaysIndex());
        rollDiceButton.setDisable(false);
        requestBuildingButton.setDisable(true);
        buyEntranceButton.setDisable(true);
        if(game.getPlayerMoneyRequestEligibility(game.getWhoPlaysIndex())){
            requestMoneyButton.setDisable(false);
        }
        else {
            requestMoneyButton.setDisable(true);
        }

    }

    private void displayCommitteeDecision(int money, int initialPrice, int finalPrice){
        Stage popup = new Stage();
        popup.setTitle("Committee's Decision");
        VBox layout = new VBox();
        Label announcement;
        int width;
        if(finalPrice == -1){
            announcement = new Label("Your request has been rejected.");
            width = announcement.getText().length();
        }
        else if(finalPrice == 0) {
            announcement = new Label("Congratulations!");
            layout.getChildren().add(announcement);
            announcement = new Label("Your request has been accepted for free!");
            width = announcement.getText().length();
        }
        else if(finalPrice == initialPrice){
            announcement = new Label("Your request has been accepted at the cost of "+finalPrice+" MLs.");
            width = announcement.getText().length();
        }
        else{
            announcement = new Label("The commitee had a bad day and it punished you by paying double.");
            layout.getChildren().add(announcement);
            width = announcement.getText().length();
            if (money >= finalPrice) {
                announcement = new Label("You will pay "+finalPrice+" MLs.");
            }
            else {
                announcement = new Label("But you don't have "+finalPrice+" MLs, so you can't make the upgrade.");
            }
        }
        width *= 8;
        layout.getChildren().add(announcement);
        layout.getChildren().add(new Label(""));
        layout.setAlignment(Pos.CENTER);

        Button button = new Button("OK");
        button.setOnAction((e)->popup.close());

        GridPane buttonGrid = new GridPane();
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(75);
        buttonGrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(25);
        buttonGrid.getColumnConstraints().add(column);

        Region space = new Region();
        space.setMinWidth(10);
        buttonGrid.add(space,0,0);
        buttonGrid.add(button,1,0);
        layout.getChildren().add(buttonGrid);

        popup.setScene(new Scene(layout, width, 100));
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();
    }

    private void displayPlayerHasNoHotels(Stage popup, String playerName){
        VBox rootNode = new VBox();
        Label label = new Label(playerName+" owns no hotels.");
        int width = 10*label.getText().length();
        rootNode.getChildren().add(label);
        rootNode.getChildren().add(new Label(""));
        rootNode.setAlignment(Pos.CENTER);

        Button button = new Button("OK");
        button.setOnAction( (e) -> popup.close());

        GridPane buttonGrid = new GridPane();
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(75);
        buttonGrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(25);
        buttonGrid.getColumnConstraints().add(column);

        Region space = new Region();
        space.setMinWidth(10);
        buttonGrid.add(space,0,0);
        buttonGrid.add(button,1,0);
        rootNode.getChildren().add(buttonGrid);
        //popup.setScene(new Scene(rootNode, 300, 100));
        popup.setScene(new Scene(rootNode, width, 80));
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();
    }

    private void displayWinner(String winner) {
        Stage popup = new Stage();
        popup.initStyle(StageStyle.UNDECORATED);
        popup.setTitle("GAME OVER");
        VBox layout = new VBox(10);
        Label label = new Label(winner + "  won the game!");
        layout.getChildren().add(label);
        label = new Label("Congratulations!!!");
        layout.getChildren().add(label);
        layout.setAlignment(Pos.CENTER);

        Button startNewGame = new Button("New Game");
        startNewGame.setOnAction((e)->{popup.close(); newGame(new ActionEvent());});
        Button quitNewGame = new Button("Quit");
        quitNewGame.setOnAction((e)->{popup.close(); closeApplication(new ActionEvent());});

        GridPane bgrid = new GridPane();
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(20);
        bgrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(20);
        bgrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(25);
        bgrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(20);
        bgrid.getColumnConstraints().add(column);
        column = new ColumnConstraints();
        column.setPercentWidth(15);
        bgrid.getColumnConstraints().add(column);

        Region space = new Region();
        space.setMinWidth(10);
        bgrid.add(space,0,0);
        space = new Region();
        space.setMinWidth(10);
        bgrid.add(space,2,0);
        bgrid.add(startNewGame,1,0);
        bgrid.add(quitNewGame,3,0);
        layout.getChildren().add(bgrid);

        popup.setScene(new Scene(layout, 550, 130));
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.showAndWait();
    }

    private boolean PayFee() {
        int cost;
        boolean ret = false;
        int player = game.getWhoPlaysIndex();
        int row = game.getPlayerRow(player);
        int col = game.getPlayerCol(player);
        Pair entrance = new Pair<>(row, col);
        int owner = game.getBoughtEntrances(entrance);
        int hotelID = game.getBoughtEntranceForHotel(entrance);
        if(hotelID != -1 && owner != -1 && owner != player){ // entrance is owned by another player
            ret = true;
            int hotelUpgradeLevel = game.getHotelUpgrades(hotelID);
            if(hotelUpgradeLevel == 0) cost = game.getDailyStayPrice(hotelID);
            if(hotelUpgradeLevel < game.getNumberOfUpgrades(hotelID)) cost = game.getUpgradedDailyStayPrice(hotelID, hotelUpgradeLevel);
            else cost = game.getFinalDailyStayPrice(hotelID);

            Double dice = Math.ceil(6*Math.random());
            int stays = dice.intValue();
            cost = stays*cost;

            int playerMoney = game.getPlayerMoney(player);
            int ownerMoney = game.getPlayerMoney(owner);

            int afford = 0;
            if (playerMoney>=cost) {
                afford = cost;
            }
            else{
                // in case player hasn't collect the money from the bank, we gave them to him/her now
                if(game.getPlayerMoneyRequestEligibility(player)) {
                    playerMoney += 1000;
                    game.setPlayerMoneyRequestEligibility(game.getWhoPlaysIndex(), false);
                    requestMoneyButton.setDisable(true);
                }

                afford = playerMoney;
                // start selling hotels to the banker. Banker will buy holding at 40% of the purchase price,
                // and will give 10% for every upgrade.

                // get player's hotels
                ArrayList<Integer> playerHotels = game.getPlayerOwnings(player);
                if (!playerHotels.isEmpty()) {
                    for (int hotel: playerHotels) {
                        int upgradeLevel = game.getHotelUpgrades(hotel);
                        int upgradesSold = 0;
                        if(upgradeLevel >= 0 ){
                            int i;
                            for(i=upgradeLevel;i>=0;i--){
                                upgradesSold += Math.floor(0.1*game.getUpgradeCost(hotel,upgradeLevel-1));
                                if (afford+upgradesSold>=cost) break;
                            }
                            game.setHotelUpgrades(hotel,i); // update hotel premises.
                        }
                        afford += upgradesSold;
                        if (afford>=cost) {
                            if (game.getHotelUpgrades(hotel) == -1) { // basic building was sold too, so get rid of entrances
                                game.freeHotelEntrancesFromPlayer(hotel, player);
                            }
                            break;
                        }
                        int purchasePrice = (int) Math.floor(0.4*game.getPurchasePrice(hotel));
                        afford += purchasePrice;
                        game.freeHotelFromPlayer(hotel, game.getWhoPlaysIndex());
                        game.setHoldingOwnedBy(hotel,-1);
                        if (afford>=cost) {
                            break;
                        }
                    }

                }
            }

            //display
            Stage popup = new Stage();
            String playerName = game.getPlayerName(player);
            popup.setTitle(playerName+"'s paying fee.");
            VBox layout = new VBox(10);
            String days;
            if(stays == 1) days="day";
            else days="days";
            Label label = new Label(playerName + " will stay at " + game.getHotelName(hotelID) + " for " + stays + " "+days+".");
            layout.getChildren().add(label);
            if (playerMoney >= cost) {
                label = new Label(playerName+" will pay "+cost+" MLs to "+game.getPlayerName(owner));
                playerMoney = playerMoney - cost;
                ownerMoney = ownerMoney + cost;
            }
            else {
                if (afford < cost) {
                    label = new Label(playerName + " could only give " + afford + " MLs out of "+cost+" MLs to " + game.getPlayerName(owner) + ".");
                    layout.getChildren().add(label);
                    label = new Label(playerName + " is bankrupt and out of the game.");
                    playerMoney = -1; // so that the player goes bankrupt (money bellow zero)
                    ownerMoney = ownerMoney + afford;
                } else {
                    label = new Label("By selling some of his/her belongings, "+playerName + " can pay " + afford + " MLs to " + game.getPlayerName(owner) + ".");
                    playerMoney = afford - cost;
                    ownerMoney += cost;
                }
            }
            layout.getChildren().add(label);
            layout.setAlignment(Pos.CENTER);

            Button button = new Button("OK");
            button.setOnAction( (e) -> popup.close());

            GridPane buttonGrid = new GridPane();
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(75);
            buttonGrid.getColumnConstraints().add(column);
            column = new ColumnConstraints();
            column.setPercentWidth(25);
            buttonGrid.getColumnConstraints().add(column);

            Region space = new Region();
            space.setMinWidth(10);
            buttonGrid.add(space,0,0);
            buttonGrid.add(button,1,0);
            layout.getChildren().add(buttonGrid);

            popup.setScene(new Scene(layout, 550, 100));
            popup.initModality(Modality.APPLICATION_MODAL);
            popup.showAndWait();

            game.setPlayerMoney(player, playerMoney);
            game.setPlayerMoney(owner, ownerMoney);
        }
        return ret;
    }

    private boolean staysOnGame (){
        int player = game.getWhoPlaysIndex();
        int money = game.getPlayerMoney(player);
        if(money >= 0) return true;
        else {
            game.playerIsOut(player);
            game.getGrid().getChildren().remove(game.getPlayerAvatar(player));
            return false;
        }
    }

    private void setButtonWasPressed () { this.buttonWasPressed = true;}

    private int costAfterBuildingCommision(int fullPrice) {
        int price;
        double chance = Math.random();
        if (chance <= 0.5f) {
            price = fullPrice;
        }
        else if (chance <= 0.7f) {
            price = -1; // request is rejected
        }
        else if (chance <= 0.85f) {
            price = 0;  // lucky winner
        }
        else {
            price = 2*fullPrice;   // tough luck
        }
        return price;
    }
}